/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.faces.support;

import java.util.Date;

/**
 *
 * @author gab
 */
public class Link {
    public String login;
    public Date data_hora;
    
    public Link(String login, Date data){
        this.login = login;
        this.data_hora = data;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getData_hora() {
        return data_hora;
    }

    public void setData_hora(Date data_hora) {
        this.data_hora = data_hora;
    }
    
    
}
