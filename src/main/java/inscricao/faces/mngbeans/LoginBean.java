/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import utfpr.faces.support.Link;
 


/**
 *
 * @author gab
 */
@ManagedBean(name="user")
@SessionScoped
public class LoginBean implements Serializable {
    public String login;
    public String senha;
    public boolean admin;
    public String message;
    public ArrayList<Link> links;

    public LoginBean() {
        this.links = new ArrayList<Link>();
    }
    
    public void addLink(String usuario){
        this.links.add(new Link(usuario, new Date()));
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    
    public String logon(){
        if(this.login.equals(this.senha)) {
            this.addLink(this.login);
            if(this.admin == true){
                return "admin";
            } else {
                return "cadastro";
            }
        }else {
            FacesContext.getCurrentInstance().addMessage("xxx", new FacesMessage("Acesso negado")); 
        }
        return "";
    }

    public ArrayList<Link> getLinks() {
        return links;
    }

    public void setLinks(ArrayList<Link> links) {
        this.links = links;
    }
    
    

}